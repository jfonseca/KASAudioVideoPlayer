/*!
 * KASAudioVideoPlayer: 1.2
 * http://www.klipartstudio.com/
 * GIT : https://gitlab.com/jfonseca/KASAudioVideoPlayer
 *
 * Copyright 2014, Klip Art Studio, LLC
 *
 * DEPENDENCIES: Greensock
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *    target: "js-backGroundAudio4",
 *    loop: false,
 *    start: 50,
 *    onComplete: complete
 * *  
 * USAGE:
 * 
 * Javascript:
 * 
 * var videoElement = new KASAudioVideoPlayer();
 * videoElement.init(this, {
 *     target: "js-backGroundAudio4",
 *     loop: false,
 *     start: 50,
 *     onComplete: complete
 * });
 * 
 * //Optional
 * function complete(obj) {
 *     console.log('Complete element  ', obj);
 * }
 * 
 * HTML Needed:
 * 
 * <audio prelaod='auto' id="js-backGroundAudio3">
 *     <source src="media/audio/JapaneseIntro.wav" type="audio/ogg">
 *     <source src="media/audio/JapaneseIntro.wav" type="audio/mpeg">
 * </audio>
 * <video width="320" height="240" preload="none" controls id="js-backGroundAudio4">
 *     <source src="media/video/Games_Final_480x270.mp4" type="video/mp4"> Your browser does not support the video tag.
 * </video>

 * isOpens() : returns a Boolean if video is opened
 * openView(int: startSeconds) : Opens video window 
 * closeView() : Closes the video window
 * updateVolume(int) : Adjust volume
 * setHasVolume(Boolean) : Set if you want volume
 * init(scope, Object) : Init the video or audio element
 * play(int:seconds) : Plays audio element default is 0 otherwise uses (int)
 * stop() : Stops the play of audio
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com
 * DATE: 02/21/2014
 */

var KASAudioVideoPlayer = function() {
  "use strict";

  var videoElement;
  var audioElement;
  var scopeObj;
  var obj_p = {};
  var videoPlaying = false;
  var animateBool = false;
  var seektime = 0;
  var alphaTween = false;
  var isOpen = false;
  var loop = false;
  var volumeObject = { volume: 0 };

  var initVolume = 0;

  var pid;
  var hasAudio = false;
  var isSafari = false;

  function openView(startFrom) {
    startFrom = startFrom == undefined ? 0.1 : startFrom;

    if (isOpen == false) {
      isOpen = true;

      console.log("TweenMax.isTweening( element ); ", videoElement);
      TweenLite.killTweensOf(videoElement);
      TweenLite.killTweensOf(volumeObject);
      TweenLite.set(videoElement, { opacity: 0, scaleY: 1 });
      videoElement.style.display = "block";
      videoElement.currentTime = startFrom;

      volumeObject.volume = 0;
      videoElement.volume = 0;

      TweenLite.to(videoElement, 0.5, { autoAlpha: 1, delay: 0.1 });

      TweenLite.to(volumeObject, 0.5, {
        volume: initVolume,
        onUpdate: fadeVolumeHandler,
        delay: 0.1
      });
      playHack();

      videoPlaying = true;
    } else {
      videoElement.currentTime = startFrom;
    }
  }

  function playHack() {
    if (isSafari == true) {
      TweenLite.delayedCall(0.1, delyaedPlay);
    } else {
      videoElement.play();
    }
  }

  function closeView() {
    if (isOpen == true) {
      isOpen = false;

      videoPlaying = false;
      TweenLite.killTweensOf(videoElement);
      TweenLite.killTweensOf(volumeObject);
      TweenLite.to(videoElement, 0.5, {
        autoAlpha: 0,
        onComplete: closeComplete
      });
      TweenLite.to(volumeObject, 0.5, {
        volume: 0,
        onUpdate: fadeVolumeHandler
      });
    }
  }

  function setHasVolume(bool) {
    hasAudio = bool;
  }

  function updateVolume(vol) {
    vol = vol > 1 ? 1 : vol;
    console.log("vol  ", vol);
    initVolume = vol;

    TweenLite.killTweensOf(volumeObject);
    TweenLite.to(volumeObject, 0.5, {
      volume: vol,
      onUpdate: fadeVolumeHandler,
      delay: 0
    });
  }

  function fadeVolumeHandler() {
    // console.log("volumeObject", volumeObject);
    if (videoElement) {
      // console.log("Video Element");
      videoElement.volume = volumeObject.volume;
    } else if (audioElement) {
      // console.log("Audio Element");
      audioElement.volume = volumeObject.volume;
    }
  }
  function isOpens() {
    return isOpen;
  }

  function init(scope, obj) {
    if (obj.target == undefined) {
      return;
    }

    scopeObj = scope;
    loop = obj.loop == undefined ? false : obj.loop;
    pid = obj.target;
    seektime = obj.start == undefined ? 0 : obj.start;

    var element = document.getElementById(pid);
    obj.element = element;
    obj_p = obj;
    // console.log(
    //   element.tagName,
    //   "element  ",
    //   element.tagName.toLowerCase().indexOf("audio")
    // );
    if (element.tagName.toLowerCase().indexOf("audio") > -1) {
      console.log("Audio Element");
      initAudio(element);
    } else if (element.tagName.toLowerCase().indexOf("video") > -1) {
      console.log("Video Element");
      initVideo(element);
    }
  }
  function initAudio(element) {
    audioElement = element;
    audioElement.addEventListener("ended", audioEndedHandler, false);
    initVolume = audioElement.volume;
    volumeObject.volume = initVolume;
    if (
      navigator.userAgent.search("Safari") >= 0 &&
      navigator.userAgent.search("Chrome") < 0
    ) {
      isSafari = true;
    }
  }
  function initVideo(element) {
    videoElement = element;
    videoElement.addEventListener("ended", videoEndedHandler, false);
    initVolume = videoElement.volume;
    volumeObject.volume = initVolume;
    TweenLite.set(videoElement, {
      autoAlpha: 0,
      scaleY: 0
    });
    videoElement.style.display = "none";
    if (
      navigator.userAgent.search("Safari") >= 0 &&
      navigator.userAgent.search("Chrome") < 0
    ) {
      isSafari = true;
    }
  }
  function upDateTimeHandler(e) {
    var vTime = videoElement.currentTime;
  }

  //  Current time

  function play(startFrom) {
    console.log("startFrom  ", startFrom);
    startFrom = startFrom == undefined ? 0 : startFrom;
    audioElement.currentTime = startFrom;
    audioElement.play();
  }
  function stop() {
    console.log("Stop  ");
    audioElement.pause();
    audioElement.currentTime = seektime;
  }
  function capture(e) {
    if (e.type == "play") {
      // trace( "play event handler ")
    }
  }

  function closeComplete() {
    videoPlaying = false;

    if (videoElement) {
      videoElement.pause();
      videoElement.currentTime = seektime;
      TweenLite.to(videoElement, 0.5, {
        autoAlpha: 0,
        onComplete: function() {
          videoElement.style.display = "none";
        }
      });
    } else if (audioElement) {
      audioElement.pause();
      audioElement.seek = seektime;
    }
  }

  function videoEndedHandler(e) {
    console.log(
      pid +
        " // video id //  video Ended Background screen, currentTime: " +
        videoElement.currentTime +
        ", seektime: " +
        seektime
    );
    if (typeof obj_p.onComplete == "function") {
      obj_p.onComplete(obj_p);
    } else if (typeof obj_p.onComplete == "string") {
      window[obj_p.onComplete](obj_p);
    }
    if (loop == true) {
      // videoElement.load();
      videoElement.currentTime = seektime;
      playHack();
      //  videoElement.play();
    } else {
    }
  }

  function audioEndedHandler(e) {
    if (loop == true) {
      audioElement.currentTime = seektime;
      audioElement.play();
    } else {
      audioElement.pause();
    }
  }

  function delyaedPlay() {
    videoElement.play();
  }

  function videoCanplayHandler() {
    //trace( " videoCanplayHandler " );
  }

  return {
    isOpens: isOpens,
    openView: openView,
    closeView: closeView,
    updateVolume: updateVolume,
    setHasVolume: setHasVolume,
    init: init,
    play: play,
    stop: stop
  };
};
