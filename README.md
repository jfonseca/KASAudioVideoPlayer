# KASAudioVideoPlayer 1.2

This componenet wasbuilt to handle mutiple audio and video files. Basically like a digital audio/video board.

## Example

To run the examples you can either upload 'Example' folder to your server or run it locally by using the npm library inside the 'Example' folder.

[View Example Online](https://www.klipartstudio.com/site/KAS_AudioVideoPlayer/)

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it

* Install the package from npm

`npm install`

* Start the local server

`npm start`

## Usage

* Greensock TweenLite required

### DESCRIPTION:

* this : Scope of view
* target : Target Element Video or Audio
* loop : Boolean true or false
* start : Play head will start at the number place here loop will also use this
* onComplete : once the audio reach the end it will call this function

Javascript code:

```javascript
var videoElement = new KASAudioVideoPlayer();
videoElement.init(this, {
    target: "js-backGroundAudio4",
    loop: false,
    start: 50,
    onComplete: complete
});

//Optional
function complete(obj) {
    console.log('Complete element  ', obj);
}
```

HTML code:

```html
<audio prelaod='auto' id="js-backGroundAudio3">
    <source src="media/audio/JapaneseIntro.wav" type="audio/ogg">
    <source src="media/audio/JapaneseIntro.wav" type="audio/mpeg">
</audio>
<video width="320" height="240" preload="none" controls id="js-backGroundAudio4">
    <source src="media/video/Games_Final_480x270.mp4" type="video/mp4"> Your browser does not support the video tag.
</video>
```

### Medthods Availible

* isOpens() : returns a Boolean if video is opened
* openView(int: startSeconds) : Opens video window 
* closeView() : Closes the video window
* updateVolume(int) : Adjust volume
* setHasVolume(Boolean) : Set if you want volume
* init(scope, Object) : Init the video or audio element
* play(int:seconds) : Plays audio element default is 0 otherwise uses (int)
* stop() : Stops the play of audio


## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASAudioVideoPlayer)

## License

MIT
